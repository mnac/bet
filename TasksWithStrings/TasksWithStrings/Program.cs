﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            FindAllSubStrings("Power");
            IsSubString("abrabarabrab", "ab");
        }

        public static void FindAllSubStrings(string input)
        {
            int n = input.Length;
            for (int i = 0; i < n; i++)
                for (int j = 1 ; j <= n-i; j++)
                {
                    Console.WriteLine(input.Substring(i, j));
                }
            
        }
        public static void IsSubString(string input,string sub)
        {
            int sub_len = sub.Length;
            for (int i = 0; i <= input.Length-sub_len; i++)
            {
                if (sub == input.Substring(i, sub_len)) Console.WriteLine(i); 
            }
            
        }
    }
}
