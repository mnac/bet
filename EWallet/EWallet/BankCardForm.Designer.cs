﻿namespace EWallet
{
    partial class BankCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.x_btn = new System.Windows.Forms.Button();
            this.bank_name_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.code_box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Pin_box = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.button1.ForeColor = System.Drawing.Color.DarkBlue;
            this.button1.Location = new System.Drawing.Point(55, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(205, 40);
            this.button1.TabIndex = 18;
            this.button1.Text = "Set Card";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // x_btn
            // 
            this.x_btn.FlatAppearance.BorderSize = 0;
            this.x_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x_btn.ForeColor = System.Drawing.Color.DarkBlue;
            this.x_btn.Location = new System.Drawing.Point(277, -1);
            this.x_btn.Name = "x_btn";
            this.x_btn.Size = new System.Drawing.Size(34, 42);
            this.x_btn.TabIndex = 19;
            this.x_btn.Text = "X";
            this.x_btn.UseVisualStyleBackColor = true;
            this.x_btn.Click += new System.EventHandler(this.x_btn_Click);
            // 
            // bank_name_box
            // 
            this.bank_name_box.BackColor = System.Drawing.Color.SteelBlue;
            this.bank_name_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bank_name_box.Location = new System.Drawing.Point(119, 67);
            this.bank_name_box.Name = "bank_name_box";
            this.bank_name_box.Size = new System.Drawing.Size(150, 26);
            this.bank_name_box.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.DarkBlue;
            this.label1.Location = new System.Drawing.Point(12, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Bank Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.DarkBlue;
            this.label2.Location = new System.Drawing.Point(12, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 23;
            this.label2.Text = "Code";
            // 
            // code_box
            // 
            this.code_box.BackColor = System.Drawing.Color.SteelBlue;
            this.code_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.code_box.Location = new System.Drawing.Point(119, 117);
            this.code_box.Name = "code_box";
            this.code_box.Size = new System.Drawing.Size(150, 26);
            this.code_box.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.DarkBlue;
            this.label3.Location = new System.Drawing.Point(12, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 20);
            this.label3.TabIndex = 25;
            this.label3.Text = "Pin";
            // 
            // Pin_box
            // 
            this.Pin_box.BackColor = System.Drawing.Color.SteelBlue;
            this.Pin_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Pin_box.Location = new System.Drawing.Point(119, 166);
            this.Pin_box.Name = "Pin_box";
            this.Pin_box.Size = new System.Drawing.Size(150, 26);
            this.Pin_box.TabIndex = 24;
            // 
            // BankCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(313, 312);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Pin_box);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.code_box);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bank_name_box);
            this.Controls.Add(this.x_btn);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BankCardForm";
            this.Text = "BankCardForm";
            this.Load += new System.EventHandler(this.BankCardForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button x_btn;
        private System.Windows.Forms.TextBox bank_name_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox code_box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Pin_box;
    }
}