﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using EWallet.Services;
namespace EWallet
{
    public partial class LoginForm : Form
    {

        private ILoginable _login;

        public LoginForm(ILoginable Login)
        {
            InitializeComponent();
            _login = Login;
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            //if input is correct and user exist close loginForm and open ViewForm that displays user's payments history
            if (_login.InputIsCorrect(username_input.Text, passwordInput.Text))
            {
                status_lbl.ForeColor = Color.Green;
                status_lbl.Text = "OK! logged in";
                _login.login(username_input.Text, passwordInput.Text);
                Close();

            }
            else
            {
                status_lbl.ForeColor = Color.Red;
                status_lbl.Text = "User not found OR input is not correct";
            }
        }

        private void Close_btn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
