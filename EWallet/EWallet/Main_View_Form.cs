﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace EWallet
{
    public partial class Main_View_Form : Form
    {
        public Main_View_Form(string path)
        {
            InitializeComponent();
            status_lbl.Text = "You have successfully logged in";
            output_txt.Text = "Select payment history";
            User.Initialize(new UserInfo(), path);
            user_lbl.Text += User.Username;
            balance_lbl.Text = balance_lbl.Text + User.Balance + "$";
            //bank_card_status.Text = "";
            DisplayCardInfo();
        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        { }
        //shopping button
        private void button2_Click(object sender, EventArgs e)
        {
            //displaying user's shopping payments history
            output_txt.Text = "";
            string[] info = User.ShopHistory.Split(' ');
            foreach (string st in info)
            {
                output_txt.Text += st;
                output_txt.Text += "\r\n";
            }

            status_lbl.Text = "Shopping payments history for last month";
        }

        private void Form1_Load(object sender, EventArgs e)
        { }

        //Other Payments button
        private void button3_Click(object sender, EventArgs e)
        {
            //displaying user's unexpected payments history
            output_txt.Text = "";
            string[] info = User.OtherPayments.Split(' ');
            foreach (string st in info)
            {
                output_txt.Text += st;
                output_txt.Text += "\r\n";
            }

            status_lbl.Text = "Unexpected payments history for last month";
        }

        private void com_btn_Click(object sender, EventArgs e)
        {
            //displaying user's communal payments history
            output_txt.Text = "";
            string[] info = User.CommmunalHistory.Split(' ');
            foreach (string st in info)
            {
                output_txt.Text += st;
                output_txt.Text += "\r\n";
            }

            status_lbl.Text = "Communal payments history for last month";
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void balance_lbl_Click(object sender, EventArgs e)
        {

        }

        private void user_lbl_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Close_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void card_opt_btn_Click(object sender, EventArgs e)
        {//making visible bank card add form
            if (User.Card == null)
                bank_card_p.Visible = true;
            else MessageBox.Show("Another Card is already added");
        }

        private void x_btn_Click(object sender, EventArgs e)
        {   //closing bank card add form
            bank_card_p.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //writing to user's data files card data
           string path = string.Format(@"../../data/{0}_data.txt", User.Username);
            UserInfo userInfo = new UserInfo();
            int Pin;
            try
            {
                Pin = int.Parse(Pin_box.Text);
            }
            catch
            {
                MessageBox.Show("Input is incorrect");
                return;
            }

            string[] st = File.ReadAllLines(path);
            string new_card = "1400" +" "+ bank_name_box.Text+" " + code_box.Text+" " + Pin.ToString();
            userInfo.WriteToFile(path,new string[]{st[0],st[1],st[2],st[3],st[4],new_card});

           //and creating card of active user reading from that file
            User.Initialize(userInfo,path);
            //displaying information about this card
            DisplayCardInfo();
            //deactivating card add form
            bank_card_p.Visible = false;

        }
        private void DisplayCardInfo()
        {
            if (User.Card != null)
            {        //displaying information about bank card
                card_box.Text = string.Format("Ballance: {0}$\r\n", User.Card.Ballance);
                card_box.Text += string.Format("Name of Bank: {0}\r\n", User.Card.NameOfBank);
                card_box.Text += string.Format("Card's code: {0}\r\n", User.Card.Code);
            }
            else card_box.Text = " No Cards Available";
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        //for deleting bank card from ewallet
        private void remove_card_btn_Click(object sender, EventArgs e)
        {
            if (User.Card != null)
            {
                User.Card = null;
                string path = string.Format(@"../../data/{0}_data.txt", User.Username);
                string[] st = File.ReadAllLines(path);
               (new UserInfo()).WriteToFile(path, new string[] { st[0], st[1], st[2], st[3], st[4]});
                DisplayCardInfo();
            }
            else MessageBox.Show("No card to remove");
        }
    }
}
