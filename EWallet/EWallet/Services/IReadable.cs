﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWallet.Services
{
    //Service for reading and getting user's data
    interface IReadable
    {
        decimal GetBalance(string path);
        string GetUserName(string path);
        string GetCommunalsHistory(string path);
        string GetShopingsHistory(string path);
        string GetOtherPaymentsHistory(string path);
        string GetCardData(string path);

    }
}
