﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWallet.Services
{
    public interface ILoginable
    {
        //for checking input data
        bool InputIsCorrect(string username, string password);
        // to log in 
        int login(string username, string password);
        //User data's path for communicating with other forms
        string Path { get; }
    }
}
