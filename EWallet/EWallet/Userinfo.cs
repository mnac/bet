﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EWallet.Services;
using System.IO;
using System.Windows.Forms;
namespace EWallet
{
    //Some implementation of ReadService
    class UserInfo : IReadable,IWriteable
    {
        public string GetUserName(string path)
        {
             
            return File.ReadAllLines(path)[0];
            
        }

        public decimal GetBalance(string path)
        {
            return decimal.Parse(File.ReadAllLines(path)[1]);
        }

        public string GetCommunalsHistory(string path)
        {
            return File.ReadAllLines(path)[2];
        }

        public string GetOtherPaymentsHistory(string path)
        {
            return File.ReadAllLines(path)[3];
        }

        public string GetShopingsHistory(string path)
        {
            return File.ReadAllLines(path)[4];
        }

        public string GetCardData(string path)
        {
            string[] data= File.ReadAllLines(path);
            if (data.Length == 6)
                return File.ReadAllLines(path)[5];
            else return "";
        }

        public void WriteToFile(string path,string[]data)
        {
            try
            {
                File.WriteAllLines(path, data);
            }
            catch(IOException e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
