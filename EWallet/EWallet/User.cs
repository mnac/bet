﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EWallet.Services;
namespace EWallet
{      //static class that represents current user's data
    static class User
    {
        //field that have every user
        public static decimal Balance { get; private set; }
        public static string Username { get; private set; }
        public static string CommmunalHistory { get; private set; }
        public static string ShopHistory { get; private set; }
        public static string OtherPayments { get; private set; }
        public static Bank_Card Card { get; set; }

        //function that initialize User's data from Server( files in hdd:) ) using some ReadService
        public static int Initialize(IReadable ReadService, string path)
        {
            Balance = ReadService.GetBalance(path);
            Username = ReadService.GetUserName(path);
            CommmunalHistory = ReadService.GetCommunalsHistory(path);
            ShopHistory = ReadService.GetShopingsHistory(path);
            OtherPayments = ReadService.GetOtherPaymentsHistory(path);
            InitializeCard(ReadService.GetCardData(path));
            return 0;
        }
        private static void InitializeCard(string data)
        {
            if (data != "")
            {
                string[] arr = data.Split(' ');
                Bank_Card temp = new Bank_Card();
                temp.Ballance = decimal.Parse(arr[0]);
                temp.Code = arr[2];
                temp.NameOfBank = arr[1];
                temp.Pin = int.Parse(arr[3]);
                Card = temp;
            }

        }








    }
}
