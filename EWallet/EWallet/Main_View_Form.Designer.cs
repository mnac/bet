﻿namespace EWallet
{
    partial class Main_View_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.user_lbl = new System.Windows.Forms.Label();
            this.balance_lbl = new System.Windows.Forms.Label();
            this.output_txt = new System.Windows.Forms.TextBox();
            this.other_btn = new System.Windows.Forms.Button();
            this.shop_btn = new System.Windows.Forms.Button();
            this.com_btn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.status_lbl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Close_btn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.card_opt_btn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.bank_card_p = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.Pin_box = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.code_box = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bank_name_box = new System.Windows.Forms.TextBox();
            this.x_btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.card_box = new System.Windows.Forms.TextBox();
            this.remove_card_btn = new System.Windows.Forms.Button();
            this.bank_card_p.SuspendLayout();
            this.SuspendLayout();
            // 
            // user_lbl
            // 
            this.user_lbl.AutoSize = true;
            this.user_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.user_lbl.ForeColor = System.Drawing.Color.DarkBlue;
            this.user_lbl.Location = new System.Drawing.Point(204, 93);
            this.user_lbl.Name = "user_lbl";
            this.user_lbl.Size = new System.Drawing.Size(65, 24);
            this.user_lbl.TabIndex = 1;
            this.user_lbl.Text = "User: ";
            this.user_lbl.Click += new System.EventHandler(this.user_lbl_Click);
            // 
            // balance_lbl
            // 
            this.balance_lbl.AutoSize = true;
            this.balance_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.balance_lbl.ForeColor = System.Drawing.Color.DarkBlue;
            this.balance_lbl.Location = new System.Drawing.Point(204, 125);
            this.balance_lbl.Name = "balance_lbl";
            this.balance_lbl.Size = new System.Drawing.Size(102, 24);
            this.balance_lbl.TabIndex = 2;
            this.balance_lbl.Text = "Ballance: ";
            this.balance_lbl.Click += new System.EventHandler(this.balance_lbl_Click);
            // 
            // output_txt
            // 
            this.output_txt.BackColor = System.Drawing.Color.Gainsboro;
            this.output_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.output_txt.Location = new System.Drawing.Point(561, 93);
            this.output_txt.Multiline = true;
            this.output_txt.Name = "output_txt";
            this.output_txt.ReadOnly = true;
            this.output_txt.Size = new System.Drawing.Size(383, 425);
            this.output_txt.TabIndex = 6;
            this.output_txt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // other_btn
            // 
            this.other_btn.BackgroundImage = global::EWallet.Properties.Resources.E_wallet;
            this.other_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.other_btn.FlatAppearance.BorderColor = System.Drawing.Color.PapayaWhip;
            this.other_btn.FlatAppearance.BorderSize = 0;
            this.other_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.other_btn.Location = new System.Drawing.Point(33, 430);
            this.other_btn.Name = "other_btn";
            this.other_btn.Size = new System.Drawing.Size(100, 85);
            this.other_btn.TabIndex = 5;
            this.other_btn.UseVisualStyleBackColor = true;
            this.other_btn.Click += new System.EventHandler(this.button3_Click);
            // 
            // shop_btn
            // 
            this.shop_btn.BackColor = System.Drawing.Color.SteelBlue;
            this.shop_btn.BackgroundImage = global::EWallet.Properties.Resources.shoping;
            this.shop_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.shop_btn.FlatAppearance.BorderSize = 0;
            this.shop_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shop_btn.Location = new System.Drawing.Point(33, 274);
            this.shop_btn.Name = "shop_btn";
            this.shop_btn.Size = new System.Drawing.Size(100, 85);
            this.shop_btn.TabIndex = 4;
            this.shop_btn.Text = " ";
            this.shop_btn.UseVisualStyleBackColor = false;
            this.shop_btn.Click += new System.EventHandler(this.button2_Click);
            // 
            // com_btn
            // 
            this.com_btn.BackColor = System.Drawing.Color.SteelBlue;
            this.com_btn.BackgroundImage = global::EWallet.Properties.Resources.communal1;
            this.com_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.com_btn.FlatAppearance.BorderColor = System.Drawing.Color.FloralWhite;
            this.com_btn.FlatAppearance.BorderSize = 0;
            this.com_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.com_btn.Location = new System.Drawing.Point(33, 125);
            this.com_btn.Name = "com_btn";
            this.com_btn.Size = new System.Drawing.Size(100, 85);
            this.com_btn.TabIndex = 3;
            this.com_btn.UseVisualStyleBackColor = false;
            this.com_btn.Click += new System.EventHandler(this.com_btn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.DarkBlue;
            this.label3.Location = new System.Drawing.Point(30, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Communal";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.DarkBlue;
            this.label1.Location = new System.Drawing.Point(58, 521);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Other";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.DarkBlue;
            this.label2.Location = new System.Drawing.Point(44, 364);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Shopping";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // status_lbl
            // 
            this.status_lbl.AutoSize = true;
            this.status_lbl.BackColor = System.Drawing.Color.Transparent;
            this.status_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.status_lbl.ForeColor = System.Drawing.Color.DarkBlue;
            this.status_lbl.Location = new System.Drawing.Point(558, 529);
            this.status_lbl.Name = "status_lbl";
            this.status_lbl.Size = new System.Drawing.Size(165, 18);
            this.status_lbl.TabIndex = 12;
            this.status_lbl.Text = "Select payments history";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Revue", 33.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(12, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(257, 55);
            this.label4.TabIndex = 13;
            this.label4.Text = "MWALLET";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Close_btn
            // 
            this.Close_btn.FlatAppearance.BorderColor = System.Drawing.Color.PapayaWhip;
            this.Close_btn.FlatAppearance.BorderSize = 0;
            this.Close_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Close_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Close_btn.ForeColor = System.Drawing.Color.DarkBlue;
            this.Close_btn.Location = new System.Drawing.Point(869, 12);
            this.Close_btn.Name = "Close_btn";
            this.Close_btn.Size = new System.Drawing.Size(75, 30);
            this.Close_btn.TabIndex = 14;
            this.Close_btn.Text = "Close";
            this.Close_btn.UseVisualStyleBackColor = true;
            this.Close_btn.Click += new System.EventHandler(this.Close_btn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.DarkBlue;
            this.label5.Location = new System.Drawing.Point(5, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 24);
            this.label5.TabIndex = 15;
            this.label5.Text = "Payments History";
            // 
            // card_opt_btn
            // 
            this.card_opt_btn.FlatAppearance.BorderSize = 0;
            this.card_opt_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.card_opt_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.card_opt_btn.ForeColor = System.Drawing.Color.DarkBlue;
            this.card_opt_btn.Location = new System.Drawing.Point(234, 170);
            this.card_opt_btn.Name = "card_opt_btn";
            this.card_opt_btn.Size = new System.Drawing.Size(239, 40);
            this.card_opt_btn.TabIndex = 16;
            this.card_opt_btn.Text = "Set Card";
            this.card_opt_btn.UseVisualStyleBackColor = true;
            this.card_opt_btn.Click += new System.EventHandler(this.card_opt_btn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.DarkBlue;
            this.label6.Location = new System.Drawing.Point(203, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(278, 29);
            this.label6.TabIndex = 17;
            this.label6.Text = "Bank Card  Information";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // bank_card_p
            // 
            this.bank_card_p.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bank_card_p.Controls.Add(this.label7);
            this.bank_card_p.Controls.Add(this.Pin_box);
            this.bank_card_p.Controls.Add(this.label8);
            this.bank_card_p.Controls.Add(this.code_box);
            this.bank_card_p.Controls.Add(this.label9);
            this.bank_card_p.Controls.Add(this.bank_name_box);
            this.bank_card_p.Controls.Add(this.x_btn);
            this.bank_card_p.Controls.Add(this.button1);
            this.bank_card_p.ForeColor = System.Drawing.SystemColors.Control;
            this.bank_card_p.Location = new System.Drawing.Point(528, 139);
            this.bank_card_p.Name = "bank_card_p";
            this.bank_card_p.Size = new System.Drawing.Size(334, 313);
            this.bank_card_p.TabIndex = 19;
            this.bank_card_p.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.DarkBlue;
            this.label7.Location = new System.Drawing.Point(25, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 20);
            this.label7.TabIndex = 33;
            this.label7.Text = "Pin";
            // 
            // Pin_box
            // 
            this.Pin_box.BackColor = System.Drawing.Color.SteelBlue;
            this.Pin_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Pin_box.Location = new System.Drawing.Point(132, 186);
            this.Pin_box.Name = "Pin_box";
            this.Pin_box.Size = new System.Drawing.Size(150, 26);
            this.Pin_box.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.DarkBlue;
            this.label8.Location = new System.Drawing.Point(25, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 20);
            this.label8.TabIndex = 31;
            this.label8.Text = "Code";
            // 
            // code_box
            // 
            this.code_box.BackColor = System.Drawing.Color.SteelBlue;
            this.code_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.code_box.Location = new System.Drawing.Point(132, 137);
            this.code_box.Name = "code_box";
            this.code_box.Size = new System.Drawing.Size(150, 26);
            this.code_box.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.DarkBlue;
            this.label9.Location = new System.Drawing.Point(25, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 20);
            this.label9.TabIndex = 29;
            this.label9.Text = "Bank Name";
            // 
            // bank_name_box
            // 
            this.bank_name_box.BackColor = System.Drawing.Color.SteelBlue;
            this.bank_name_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bank_name_box.Location = new System.Drawing.Point(132, 87);
            this.bank_name_box.Name = "bank_name_box";
            this.bank_name_box.Size = new System.Drawing.Size(150, 26);
            this.bank_name_box.TabIndex = 28;
            // 
            // x_btn
            // 
            this.x_btn.FlatAppearance.BorderSize = 0;
            this.x_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x_btn.ForeColor = System.Drawing.Color.DarkBlue;
            this.x_btn.Location = new System.Drawing.Point(290, 19);
            this.x_btn.Name = "x_btn";
            this.x_btn.Size = new System.Drawing.Size(34, 42);
            this.x_btn.TabIndex = 27;
            this.x_btn.Text = "X";
            this.x_btn.UseVisualStyleBackColor = true;
            this.x_btn.Click += new System.EventHandler(this.x_btn_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.button1.ForeColor = System.Drawing.Color.DarkBlue;
            this.button1.Location = new System.Drawing.Point(68, 260);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(205, 40);
            this.button1.TabIndex = 26;
            this.button1.Text = "Set Card";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // card_box
            // 
            this.card_box.BackColor = System.Drawing.Color.SteelBlue;
            this.card_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.card_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.card_box.ForeColor = System.Drawing.Color.DarkBlue;
            this.card_box.Location = new System.Drawing.Point(208, 296);
            this.card_box.Multiline = true;
            this.card_box.Name = "card_box";
            this.card_box.ReadOnly = true;
            this.card_box.Size = new System.Drawing.Size(273, 144);
            this.card_box.TabIndex = 20;
            // 
            // remove_card_btn
            // 
            this.remove_card_btn.FlatAppearance.BorderSize = 0;
            this.remove_card_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.remove_card_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.remove_card_btn.ForeColor = System.Drawing.Color.DarkBlue;
            this.remove_card_btn.Location = new System.Drawing.Point(230, 451);
            this.remove_card_btn.Name = "remove_card_btn";
            this.remove_card_btn.Size = new System.Drawing.Size(239, 40);
            this.remove_card_btn.TabIndex = 21;
            this.remove_card_btn.Text = "Remove Card";
            this.remove_card_btn.UseVisualStyleBackColor = true;
            this.remove_card_btn.Click += new System.EventHandler(this.remove_card_btn_Click);
            // 
            // Main_View_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(960, 556);
            this.Controls.Add(this.remove_card_btn);
            this.Controls.Add(this.card_box);
            this.Controls.Add(this.bank_card_p);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.card_opt_btn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Close_btn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.status_lbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.output_txt);
            this.Controls.Add(this.other_btn);
            this.Controls.Add(this.shop_btn);
            this.Controls.Add(this.com_btn);
            this.Controls.Add(this.balance_lbl);
            this.Controls.Add(this.user_lbl);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main_View_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MWALLET";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.bank_card_p.ResumeLayout(false);
            this.bank_card_p.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label user_lbl;
        private System.Windows.Forms.Label balance_lbl;
        private System.Windows.Forms.Button com_btn;
        private System.Windows.Forms.Button shop_btn;
        private System.Windows.Forms.Button other_btn;
        private System.Windows.Forms.TextBox output_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label status_lbl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Close_btn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button card_opt_btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel bank_card_p;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Pin_box;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox code_box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox bank_name_box;
        private System.Windows.Forms.Button x_btn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox card_box;
        private System.Windows.Forms.Button remove_card_btn;
    }
}

