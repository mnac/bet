﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWallet
{
    class Bank_Card
    {
        public decimal Ballance { get; set; }
        public string NameOfBank { get; set; }
        public string Code { get; set; }
        public int Pin { get; set; }
    }
}
