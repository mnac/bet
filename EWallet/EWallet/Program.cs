﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using EWallet.Services;
namespace EWallet
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ILoginable log = new Log_in();
            //running loginform
            Application.Run(new LoginForm(log));
            if (log.Path != string.Empty)
            {   //passing user data's path into viewform from loginForm 
                Application.Run(new Main_View_Form(log.Path));
            }



        }
    }
}
