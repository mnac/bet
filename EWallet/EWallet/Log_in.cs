﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EWallet.Services;
using System.Windows.Forms;
namespace EWallet
{
    //Some implementation of Log into account Service  
    class Log_in : ILoginable
    {
        private const string _loginData = @"../../data/LoginData.txt";

        public string Path { private set; get; } = string.Empty;
        //this function checks input data;
        public bool InputIsCorrect(string username, string password)
        {
            if (username == "" || password == "") return false;
            return File.ReadAllText(_loginData).Contains(username + "-" + password);
        }

        //this function returns direction of user's data files
        public int login(string username, string password)
        {
            if (!InputIsCorrect(username, password)) return -1;
            Path = string.Format(@"../../data/{0}_data.txt",username);
            return 0;
        }


    }
}
