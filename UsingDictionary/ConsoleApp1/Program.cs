﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = {1,1,2,2,3,8,3};
             

            Console.WriteLine(FindAloneNumber(a)); 
        }
        public static int FindAloneNumber(int[] arr)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();

            for(int i = 0; i < arr.Length; i++)
            {
                if (dict.ContainsKey(arr[i]))
                    dict[arr[i]]++;
                else dict.Add(arr[i], 1);
            }

            return dict.FirstOrDefault(x => x.Value == 1).Key;
        }
    }
}
