﻿using System;

namespace Homework2
{
    class Program
    {
        static void Main(string[] args)
        {
           
            int[] arr = { 10, 15, 20, 30, 5, 6, 7, 8 };
            Console.WriteLine(CountPairs(arr,7));
            string temp = "abcba";
            Console.WriteLine(IsPolindrome(temp));
            Console.ReadLine();
        }
    
        public static bool IsPolindrome(string input)
        {
            int n = input.Length;
            for (int i = 0; i <n/2; i++)
            {
                if (input[i] != input[n - i - 1]) return false;
            }
            return true;
        }

        public static int CountPairs(int[] input,int m)
        {
            int n = input.Length;
           
            // array for remainders
            int[] remainders = new int[m];

            // Count occurrences of all remainders 
            for (int i = 0; i < n; i++)
                 remainders[input[i] % m]++;

            // If both pairs are divisible by 'm' 
            int count = remainders[0] * (remainders[0] - 1) / 2;

            // count for all i and (m-i) pairs
            
            for (int i = 1; i <= m / 2 && i != (m - i); i++)
                count += remainders[i] * remainders[m - i];

            // If m is even 
            if (m % 2 == 0)
                count += (remainders[m / 2] * (remainders[m / 2] - 1) / 2);

            return count;
        }

    }
}
