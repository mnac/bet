﻿CREATE TABLE [dbo].[staffs]
(
	[staff Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[first name] VARCHAR(50) NOT NULL, 
    [last name] VARCHAR(50) NULL, 
	[phone] VARCHAR(30) NULL, 
    [email] VARCHAR(50) NOT NULL, 
    [store id] INT NOT NULL,
  FOREIGN KEY ([store id]) REFERENCES [stores]([store id])
   on delete no action
   on update no action,
)
