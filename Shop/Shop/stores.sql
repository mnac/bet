﻿CREATE TABLE [dbo].[stores]
(
	[store id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [store name] VARCHAR(50) NOT NULL, 
    [phone] VARCHAR(30) NOT NULL, 
    [email] VARCHAR(50) NOT NULL, 
    [country] VARCHAR(50) NULL,
	[city] VARCHAR(30) NULL, 
    [street] VARCHAR(200) NULL, 
    [post code] VARCHAR(5) NULL
)

