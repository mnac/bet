﻿CREATE TABLE [dbo].[order items]
(
	[item id] INT NOT NULL PRIMARY KEY IDENTITY,
	[order id] INT NOT NULL,
    [product id] INT NOT NULL,
 
 [quantity] INT NOT NULL,
 [total price] DECIMAL (10, 2) NOT NULL,
 
 FOREIGN KEY ([order id]) references orders([order id])
  on delete cascade
  on update cascade,

FOREIGN KEY ([product id]) references products([product id])
   on delete cascade
  on update cascade
)

