﻿CREATE TABLE [dbo].[products]
(
	[product id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [product name] VARCHAR(50) NULL, 
    [category id] INT NOT NULL, 
    [producer id] INT NOT NULL, 
    [description] VARCHAR(200) NULL, 
    [retail price] MONEY NOT NULL, 
    [wholesale price] MONEY NOT NULL,
	[available] BIT NOT NULL, 
    [code] NCHAR(10) NULL, 
    [number] INT NOT NULL, 
    [min number] INT NOT NULL, 
    FOREIGN KEY ([category id]) REFERENCES [categories]([category id])
	on update cascade
	on delete cascade,
	FOREIGN KEY ([producer id]) REFERENCES [producers]([producer id])
	on update cascade,
)
