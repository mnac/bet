﻿CREATE TABLE [dbo].[Customers]
(
	[customer id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [first name] VARCHAR(50) NOT NULL, 
    [last name] VARCHAR(50) NULL, 
	[ballance] INT NOT NULL,
    [phone] VARCHAR(30) NULL, 
    [email] VARCHAR(50) NOT NULL, 
    [country] VARCHAR(50) NULL,
	[city] VARCHAR(30) NULL, 
    [street] VARCHAR(200) NULL, 
    [post code] VARCHAR(5) NULL
)
