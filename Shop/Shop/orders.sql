﻿CREATE TABLE [dbo].[orders]
(
	[order id] INT NOT NULL PRIMARY KEY IDENTITY,
	[customer id] INT NOT NULL,
 [order status] tinyint NOT NULL,
 -- Order status: 1 = Pending; 2 = Processing; 3 = Rejected; 4 = Completed
 [order date] DATE NOT NULL,
 [required date] DATE NULL,
 [shipped date] DATE NULL,
 [store id] INT NOT NULL,
 [staff id] INT NOT NULL,
 FOREIGN KEY ([customer id]) REFERENCES customers([customer id])
        on delete cascade
		on update cascade,
 FOREIGN KEY ([store id]) REFERENCES stores([store id])
        on delete cascade
		on update cascade,
FOREIGN KEY ([staff id]) REFERENCES staffs([staff id])
        on delete cascade
		on update cascade,

 )
