﻿CREATE TABLE [dbo].[categories]
(
	[category id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [category name] VARCHAR(50) NOT NULL
)
