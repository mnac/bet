﻿CREATE TABLE [dbo].[producers]
(
	[producer id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [producer name] VARCHAR(50) NOT NULL
)
