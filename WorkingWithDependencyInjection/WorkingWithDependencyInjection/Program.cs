﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithDependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel(new Bindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            var DrawService = kernel.Get<IDrawable>();
            Board board = new Board(DrawService);
            board.DrawLine(6);
            board.DrawRectangle(7, 8);
            board.DrawTriangle(10);
        }
    }
}
