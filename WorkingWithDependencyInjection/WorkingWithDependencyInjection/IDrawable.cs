﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithDependencyInjection
{
    interface IDrawable
    {
        void DrawTriangle(int height);
        void DrawRectangle(int length,int width);
        void DrawLine(int length);
    }
}
