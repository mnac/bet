﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithDependencyInjection
{
    class DrawService : IDrawable
    {



        public void DrawLine(int length)
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine("\n");

        }

        public void DrawRectangle(int length, int width)
        {
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");
        }

        public void DrawTriangle(int height)
        {
            for (int i = 0; i < height; i++)
            {
                for (int k = height-i; k >=0; k--)
                {
                    Console.Write(' ');
                }
                for (int j = 0; j < 2 * i + 1; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");
        }
    }
}
