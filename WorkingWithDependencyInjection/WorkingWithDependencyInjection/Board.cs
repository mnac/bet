﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithDependencyInjection
{
    class Board
    {
        private IDrawable _draw;
        public Board(IDrawable service)
        {
            _draw = service;
        }

        public void DrawLine(int length) => _draw.DrawLine(length);

        public void DrawRectangle(int length, int width) => _draw.DrawRectangle(length, width);

        public void DrawTriangle(int height) => _draw.DrawTriangle(height);
    }
}
