﻿using Ninject.Modules;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithDependencyInjection
{
    class Bindings : NinjectModule
    {
        public override void Load()
        => Bind<IDrawable>().To<DrawService>();
    }
}
