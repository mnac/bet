﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
   public class Car
    {
        public int Id { get; set; }
       // [ForeignKey("Firm")]
        public int FirmId { get; set; }
        public Firm firm { get; set; }
        public string Model { get; set; }
        public DateTime CreationDate { get; set; }
        public int MaxSpeed { get; set; }
    }
}
