﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Models
{
   public class Firm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public DateTime Founded { get; set; }
        public decimal Stocks { get; set; }
    }
}
