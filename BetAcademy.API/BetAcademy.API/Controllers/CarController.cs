﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using DataTransferObject.DTOs;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BetAcademy.API.Controllers
{
   [Route("cars")]
    //[ApiController]
    public class CarController : ControllerBase
    {
        ICarRepository _carRepository;
        private readonly ILog _logger;
        public CarController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
            _logger = LogManager.GetLogger(typeof(CarController));
        }
        [HttpGet()]
        public IEnumerable<CarDTO> GetAllCars()
        {
            _logger.Info("********************GetAllCars Started********************");
           return  _carRepository.GetAllCars();
        }
        [HttpGet]
        [Route("{Id}")]
        public CarDTO Get(int Id)
        {
            return _carRepository.GetCar(Id);
        }
    }
}