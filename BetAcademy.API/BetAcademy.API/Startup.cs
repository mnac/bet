using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Bootstrap;
using BusinessLogicLayer.AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Interfaces.Implementations;
using DataAccessLayer;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BetAcademy.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Injection.RegisterLogger();
            //var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());

            //XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           // services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddControllers();
            services.AddTransient<ICarRepository, CarsRepository>();
            services.AddTransient<Context,Context >();

            var ioc = new AutoMapperModule();
            ioc.Load();
            var mapper = ioc.GetMapper();
            services.AddSingleton(mapper);
            services.AddAutoMapper(typeof(Startup));
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
