﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using AutoMapper;
using DataAccessLayer.Models;
using DataTransferObject.DTOs;
using Ninject;
using Ninject.Modules;

namespace BusinessLogicLayer.AutoMapper
{
  public  class AutoMapperModule : NinjectModule
    {
        public StandardKernel Nut { get; set; }
        public override void Load()
        {
            Nut = new StandardKernel();
           
            var mapperConfiguration = new MapperConfiguration(cfg => { CreateConfiguration(); });
            Nut.Bind<IMapper>().ToConstructor(c => new Mapper(mapperConfiguration)).InSingletonScope();
        }

        public IMapper GetMapper()
        {
            return Nut.Get<IMapper>();
        }

        public MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Car, CarDTO>().ForAllMembers(x => x.UseDestinationValue());
                cfg.AddProfile(new AutoMapping());
                cfg.AddMaps(Assembly.Load("DataTransferObject"));
                cfg.AddMaps(Assembly.Load("DataAccessLayer"));

                cfg.AddMaps(Assembly.GetExecutingAssembly());
              //  Nut.Load(Assembly.GetExecutingAssembly()); Nut.Load(Assembly.Load("DataAccessLayer"));
               // cfg.AddMaps(Assembly.GetExecutingAssembly());
                // cfg.AddMaps(Assembly.Load("DataAccessLayer"));
            });
            
            return config;
        }

    }
}
