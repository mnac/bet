﻿using System;
using System.Collections.Generic;
using System.Text;
using DataTransferObject.DTOs;
using DataAccessLayer;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;
using DataAccessLayer.Models;
using BusinessLogicLayer.AutoMapper;
namespace BusinessLogicLayer.Interfaces.Implementations
{
   public class CarsRepository : ICarRepository
    {
        private Context _dbcontext;
        private IMapper _mapper;
        public CarsRepository(Context context,IMapper mapper)
        {
            _dbcontext = context;
            _mapper = mapper;
        }
        public void Create(CarDTO Car)
        {
           
        }

        public void Delete(CarDTO Car)
        {
            
        }

        public IEnumerable<CarDTO> GetAllCars()
        {
            //using AutoMapper I get exception (((

            //var cars = _dbcontext.Cars.ToList();
            //var mappedcars = _mapper.Map<List<CarDTO>>(cars);
            //return mappedcars;

            List<CarDTO> dTOs = new List<CarDTO>();
            var cars = _dbcontext.Cars.ToList();
            foreach (Car car in cars)
            {
                dTOs.Add(new CarDTO() { CreationDate = car.CreationDate, MaxSpeed = car.MaxSpeed, Model = car.Model });
            }
            return dTOs;
        }
        
        public CarDTO GetCar(int CarId)
        {
            var car = _dbcontext.Cars.FirstOrDefault(x => x.Id == CarId);
            if(car == null)
            {
                throw new Exception($"Car with ID:{CarId} doesn't exist");
            }
            // return _mapper.Map<CarDTO>(car);

            return new CarDTO() { CreationDate = car.CreationDate, MaxSpeed = car.MaxSpeed, Model = car.Model };
        }
    }
}
