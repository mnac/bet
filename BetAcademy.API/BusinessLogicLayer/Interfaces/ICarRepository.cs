﻿using DataTransferObject.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interfaces
{
  public  interface ICarRepository
    {
        IEnumerable<CarDTO> GetAllCars();
        CarDTO GetCar(int CarId);
        void Create(CarDTO Car);
        void Delete(CarDTO Car);
    }
}
