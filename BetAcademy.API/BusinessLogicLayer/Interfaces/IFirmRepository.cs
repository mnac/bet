﻿using DataTransferObject.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interfaces
{
  public  interface IFirmRepository
    {
        IEnumerable<FirmDTO> GetAllFirms();
        FirmDTO GetFirm(int FirmId);
        void Create(FirmDTO Firm);
        void Delete(FirmDTO Firm);
    }
}
