﻿using log4net;
using log4net.Config;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Bootstrap
{
   public static class Injection
    {
        public static void RegisterLogger()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            string solutiondir = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
            XmlConfigurator.Configure(logRepository, new FileInfo(solutiondir + "\\" + "Bootstrap" + "\\log4net.config"));
        }
    }
}
