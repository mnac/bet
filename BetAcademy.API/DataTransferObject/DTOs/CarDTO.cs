﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObject.DTOs
{
   public class CarDTO
    {
        public int Id { get; set; }
       // [ForeignKey("Firm")]
        public int FirmId { get; set; }
        public Firm firm { get; set; }
        public string Model { get; set; }
        public DateTime CreationDate { get; set; }
        public int MaxSpeed { get; set; }
    }
}
