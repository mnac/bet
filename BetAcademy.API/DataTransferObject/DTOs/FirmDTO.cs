﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObject.DTOs
{
   public class FirmDTO
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public DateTime Founded { get; set; }
    }
}
