﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Node<T> where T : IComparable
    {
        public T Data { get; set; }
        public Node<T> Next { get; set; }
        public Node(T data,Node<T> next)
        {
            Data = data;
            Next = next;
        }
    }

    class LinkedList<T> where T:IComparable
    {
        Node<T> Head, Last;
        int Length;
        public LinkedList()
        {
            Last = Head = null;
            Length = 0;
        }
        public void  Add(T data)
        {
            if (Length == 0)
            {
                Head = new Node<T>(data, null);
                Last = Head;
                Length++;
            }
            else
            {
                Node<T> item = new Node<T>(data, null);
                Last.Next = item;
                Last = item;
                Length++; 
            }
            
        }
        public void AddFromBeginning(T data)
        {
            Node<T> item = new Node<T>(data, Head);
            Head = item;
            Length++;
        }
        public void Show()
        {
            Console.WriteLine("\n");
            Node<T> temp=Head;
            while (temp!=null)
            {  
                Console.Write(temp.Data+"->");
                temp = temp.Next;
            }
        }
        public void AddAfter(Node<T> node,T data)
        {
            Node<T> temp = Head;
            while (temp != node)
            {
                temp = temp.Next;
            }
            Node<T> toadd = new Node<T>(data, temp.Next);
            temp.Next = toadd;
        }

        public int Remove(T data)
        {
            
            int count = 0;
           while(Head.Data.Equals(data))
            {
                Head = Head.Next;
                count++;
            }
            Node<T> temp = Head;
            Node<T> temp2 = temp.Next;
            while (temp2 != null)
            {
                if (temp2.Data.Equals(data))
                {
                    temp.Next = temp2.Next;
                    temp = temp2;
                    temp2 = temp.Next;
                    count++;
                    continue;
                }
                temp = temp.Next;
                temp2 = temp2.Next;
            }
            Length -= count;
            return count;

        }
        public void Remove(Node<T> node)
        {
            if (Head == node) {Head = Head.Next; Length--; return;}
             
            Node<T> temp = Head;
            Node<T> temp2 = temp.Next;
            while (temp2 != null)
            {
                if (temp2==node)
                {
                    temp.Next = temp2.Next;
                    temp = temp2;
                    Length--;
                    break;
                }
                temp = temp.Next;
                temp2 = temp2.Next;
            }
        }
        public void Reverse()
        {
            Stack<Node<T>> stack = new Stack<Node<T>>(Length);
            Node<T> temp = Head;
            while (temp != null)
            {
                stack.Push(temp);
                temp = temp.Next;
            }
            Head = stack.Pop();
            temp = Head;
            while (stack.Count != 0)
            {
                temp.Next = stack.Pop();
                temp = temp.Next;
            }
            Last = temp;
            Last.Next = null;
        }
        public static void Sort(LinkedList<int> list)
        {
            Node<int> temp, min;
            int len = list.Length;
            LinkedList<int> newList = new LinkedList<int>();
            for (int i = 0; i < len; i++)
            {
                temp = list.Head;
                min = list.Head;
                while (temp != null)
                {
                    if (min.Data > temp.Data) { min = temp; }
                    temp = temp.Next;

                }
                if (min != null) newList.Add(min.Data);
               if(min!=null) list.Remove(min);
                
            }
            list.Head = newList.Head;
        }
        public void Distinct()
        {
            HashSet<T> unique = new HashSet<T>();
            Node<T> temp = Head;
            unique.Add(Head.Data);
            while (temp.Next != null)
            {
                if (!unique.Add(temp.Next.Data))
                {
                    //Remove(temp.Next);
                    temp.Next = temp.Next.Next;
                }
                else temp = temp.Next;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<int> list= new LinkedList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(13);
            list.Add(32);
            list.Add(322);
            list.Add(31);
            list.Add(33);
            list.Add(33);

            list.Show();
            list.Distinct();
            list.Show();

            Console.Read();

        }
    }
}
