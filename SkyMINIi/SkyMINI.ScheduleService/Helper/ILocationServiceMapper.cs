﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SkyMINI.ScheduleService.DAL.Models;
using SkyMINI.ScheduleService.Domain.Models;

namespace SkyMINI.ScheduleService.Helper
{
    public interface ILocationServiceMapper
    {
        LocationDTO LocationToDTO(Location location);
        Location LocationToDAO(LocationDTO locationDto);
    }
}
