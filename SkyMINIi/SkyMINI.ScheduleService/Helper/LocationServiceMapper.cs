﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SkyMINI.ScheduleService.DAL.Models;
using SkyMINI.ScheduleService.Domain.Models;

namespace SkyMINI.ScheduleService.Helper
{
    public class LocationServiceMapper : ILocationServiceMapper
    {
        public Location LocationToDAO(LocationDTO locationDto)
        {
            if (locationDto != null)
            {
                Location location = new Location
                {
                    Id = locationDto.Id,
                    AirportCode = locationDto.AirportCode,
                    AirportName = locationDto.AirportName,
                    City = locationDto.City,
                    Country = locationDto.Country,
                    Latitude = locationDto.Latitude,
                    Longitude = locationDto.Longitude
                };

                return location;
            }

            return new Location();
         
        }

        public LocationDTO LocationToDTO(Location location)
        {
            if (location != null)
            {
                LocationDTO locationDto = new LocationDTO
                {
                    Id = location.Id,
                    AirportCode = location.AirportCode,
                    AirportName = location.AirportName,
                    City = location.City,
                    Country = location.Country,
                    Latitude = location.Latitude,
                    Longitude = location.Longitude
                };

                return locationDto;
            }

            return new LocationDTO();
        }
    }
}
