﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AirplaneService.Models.DB;
using SkyMINI.ScheduleService.DAL.Models;
using SkyMINI.ScheduleService.Domain.Models;

namespace SkyMINI.ScheduleService.Helper
{
    public class ScheduleServiceMapper : IScheduleServiceMapper
    {
        private readonly ILocationServiceMapper _locationMapper;

        public ScheduleServiceMapper(ILocationServiceMapper locationMapper)
        {
            _locationMapper = locationMapper;
        }

        public ScheduleDTO ScheduleToDTO(Schedule schedule)
        {
            bool statusParsed = Enum.TryParse(typeof(Status), schedule.Status, out var result);

            ScheduleDTO scheduleDto = new ScheduleDTO
            {
                Id = schedule.Id,
                Departure = schedule.Departure,
                Arrival = schedule.Departure,
                Airplane = GetAirplaneById(schedule.AirPlaneId.Value).Result,
                Status = statusParsed ? (Status)result : Status.Unkonwn,
                OriginLocation = _locationMapper.LocationToDTO(schedule.OriginLocation),
                DestinationLocation = _locationMapper.LocationToDTO(schedule.DestinationLocation)
            };

            return scheduleDto;
        }

        public Schedule ScheduleToDAO(ScheduleDTO scheduleDto)
        {

            Schedule scheduleDao = new Schedule
            {
                Id = scheduleDto.Id,
                Departure = scheduleDto.Departure,
                Arrival = scheduleDto.Departure,
                AirPlaneId = scheduleDto?.Airplane?.AirplaneId,
                OriginLocationId = scheduleDto.OriginLocation.Id,
                DestinationLocationId = scheduleDto.DestinationLocation.Id,
                Status = scheduleDto.Status.ToString(),
                OriginLocation = _locationMapper.LocationToDAO(scheduleDto.OriginLocation),
                DestinationLocation = _locationMapper.LocationToDAO(scheduleDto.DestinationLocation)
            };

            return scheduleDao;
        }

        private async Task<Airplane> GetAirplaneById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44303/api/Airplane/");
                var response = await client.GetAsync(id.ToString());
                return response.IsSuccessStatusCode ? await response.Content.ReadAsAsync<Airplane>() : null;
            }
        }
    }
}
