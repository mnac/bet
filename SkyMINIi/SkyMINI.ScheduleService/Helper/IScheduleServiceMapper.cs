﻿using SkyMINI.ScheduleService.DAL.Models;
using SkyMINI.ScheduleService.Domain.Models;

namespace SkyMINI.ScheduleService.Helper
{
    public interface IScheduleServiceMapper
    {
        ScheduleDTO ScheduleToDTO(Schedule schedule);
        Schedule ScheduleToDAO(ScheduleDTO schedule);
    }
}
