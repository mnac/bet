﻿using System;
using System.Collections.Generic;

namespace SkyMINI.ScheduleService.DAL.Models
{
    public partial class Location
    {
        public Location()
        {
            ScheduleDestinationLocation = new List<Schedule>();
            ScheduleOriginLocation = new List<Schedule>();
        }

        public int Id { get; set; }
        public string AirportCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string AirportName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public ICollection<Schedule> ScheduleDestinationLocation { get; set; }
        public ICollection<Schedule> ScheduleOriginLocation { get; set; }
    }
}
