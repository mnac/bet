﻿using Microsoft.EntityFrameworkCore;

namespace SkyMINI.ScheduleService.DAL.Models
{
    public partial class ScheduleDbContext : DbContext
    {
        public ScheduleDbContext()
        {
        }

        public ScheduleDbContext(DbContextOptions<ScheduleDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.AirportCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Latitude)
                    .IsRequired();

                entity.Property(e => e.Longitude)
                    .IsRequired();

                entity.Property(e => e.AirportName)
                    .HasColumnName("Airport_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(50);
            });

            modelBuilder.Entity<Schedule>(entity =>
            {
                entity.Property(e => e.Arrival).HasColumnType("datetime");

                entity.Property(e=> e.Status).HasMaxLength(20);

                entity.Property(e => e.Departure).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("Status");

                entity.Property(e => e.DestinationLocationId).HasColumnName("Destination_Location_Id");

                entity.Property(e => e.OriginLocationId).HasColumnName("Origin_Location_Id");

                entity.HasOne(d => d.DestinationLocation)
                    .WithMany(p => p.ScheduleDestinationLocation)
                    .HasForeignKey(d => d.DestinationLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Destination_Location_Id");

                entity.HasOne(d => d.OriginLocation)
                    .WithMany(p => p.ScheduleOriginLocation)
                    .HasForeignKey(d => d.OriginLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Origin_Location_Id");
            });
        }

    }
}
