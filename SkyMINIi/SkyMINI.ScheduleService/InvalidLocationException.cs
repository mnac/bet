﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyMINI.ScheduleService
{
    [Serializable]
    public class InvalidLocationException : Exception
    {
        public InvalidLocationException(){ }

        public InvalidLocationException(string message) : base(message){ }

        public InvalidLocationException(string message, Exception inner) : base(message, inner){ }
    }
}
