﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SkyMINI.ScheduleService.DAL.Models;
using SkyMINI.ScheduleService.Domain.Models;
using SkyMINI.ScheduleService.Helper;


namespace SkyMINI.ScheduleService.Controllers
{
    [Route("api/[controller]")]
    public class SchedulesController : Controller
    {
        private readonly ScheduleDbContext _context;
        private readonly IScheduleServiceMapper _mapper;
        private readonly ILogger _logger;

        public SchedulesController(ScheduleDbContext context,
                                 IScheduleServiceMapper mapper,
                                 ILogger<SchedulesController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAllSchedules()
        {

            var schedules = _context.Schedule
                .Include(s => s.OriginLocation)
                .Include(s => s.DestinationLocation);

            if (schedules == null || !schedules.Any())
            {
                _logger.LogInformation("Schedule data is null or empty.");
                return NotFound("No schedule available");
            }

            else
            {
                List<ScheduleDTO> dtos = new List<ScheduleDTO>();

                foreach (var schedule in schedules)
                {
                    if (schedule != null)
                    {
                        var scheduleDto = _mapper.ScheduleToDTO(schedule);
                        dtos.Add(scheduleDto);
                    }
                }
                return new JsonResult(dtos);
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetSchedule([FromRoute] int? id)
        {
            if (!id.HasValue)
            {
                _logger.LogInformation("Schedule id was null");
                return NotFound("Non existing schedule specified");
            }

            else
            {
                var schedule = _context.Schedule
                    .Include(s => s.OriginLocation)
                    .Include(s => s.DestinationLocation)
                    .FirstOrDefault(s => s.Id == id.Value);

                if (schedule == null)
                {
                    _logger.LogInformation("Schedule was null");
                    return NotFound("The requested schedule is not found");
                }

                else
                {
                    var scheduleDto = _mapper.ScheduleToDTO(schedule);
                    return new JsonResult(scheduleDto);
                }
            }
        }

        [HttpPost]
        public IActionResult AddSchedule([FromBody] ScheduleDTO schedule)
        {
            if (schedule == null)
            {
                _logger.LogInformation("Schedule was null.");
                return BadRequest("Can not add empty schedule.");
            }

            // get origin location by id from db
            var origin = _context.Location.SingleOrDefault(l => l.Id == schedule.OriginLocation.Id);
            if (origin == null)
            {
                _logger.LogInformation("No matching origin id found.");
                return BadRequest("Origin location missing.");
            }

            //get destination location by id from db
            var destination = _context.Location.SingleOrDefault(d => d.Id == schedule.DestinationLocation.Id);
            if (destination == null)
            {
                _logger.LogInformation("No matching destination id found.");
                return BadRequest("Destination location missing.");
            }

            var scheduleDao = _mapper.ScheduleToDAO(schedule);
            scheduleDao.OriginLocation = origin;
            scheduleDao.DestinationLocation = destination;

            _context.Schedule.Add(scheduleDao);

            try
            {
                _context.SaveChanges();
            }

            catch (DbUpdateException e) when (schedule.Id != 0)
            {
                string message = "Can not add already on existing schedule, use update schedule if needed";
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                _logger.LogError(message);
                throw new InvalidScheduleException(message, e);
            }

            catch (DbUpdateException e)
            {
                string message = "The schedule to add is incomplete, ensure to complete all required fields.";
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidScheduleException(message, e);
            }

            //In majority of cases wont execute.
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidOperationException("The schedule can not be added", e);
            }

            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateSchedule([FromBody] ScheduleDTO schedule)
        {
            if (schedule == null)
            {
                _logger.LogInformation("Schedule was null");
                return BadRequest("Can not update with empty schedule.");
            }

            var scheduleToUpdate = _context.Schedule.FirstOrDefault(s => s.Id == schedule.Id);

            if (scheduleToUpdate == null)
            {
                _logger.LogInformation("No schedule found for the id specified.");
                return BadRequest("The requested schedule is not found.");
            }

            if (!_context.Location.Any(ol => ol.Id == schedule.OriginLocation.Id) ||
                !_context.Location.Any(dl => dl.Id == schedule.DestinationLocation.Id))
            {
                _logger.LogInformation("Origin or destination location with id specified are not found.");
                return BadRequest("Locations spcified does not exist.");
            }

            _context.Entry(scheduleToUpdate).State = EntityState.Detached;
            scheduleToUpdate = _mapper.ScheduleToDAO(schedule);
            _context.Entry(scheduleToUpdate).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }

            catch (DbUpdateException e)
            {
                string message = "The schedule to update is incomplete, ensure to complete all required fields.";
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidScheduleException(message, e);
            }

            //In majority of cases wont execute.
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidScheduleException("The schedule can not be updated", e);
            }

            return Ok();
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteSchedule([FromRoute] int? id)
        {
            if (!id.HasValue)
            {
                _logger.LogInformation("Schedule id was null.");
                return BadRequest("Non existing schedule id specified");
            }

            var scheduleToDelete = _context.Schedule.FirstOrDefault(s => s.Id == id.Value);

            if (scheduleToDelete == null)
            {
                _logger.LogInformation("No matching schedule found for id specified.");
                return NotFound("The requested schedule not found");
            }

            _context.Entry(scheduleToDelete).State = EntityState.Deleted;
            _context.Schedule.Remove(scheduleToDelete);

            try
            {
                _context.SaveChanges();
            }

            catch (DbUpdateException e)
            {
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidOperationException("Delete operation can not be performed", e);
            }

            return Ok();
        }
    }
}
