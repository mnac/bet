﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using SkyMINI.ScheduleService.DAL.Models;
using SkyMINI.ScheduleService.Domain.Models;
using SkyMINI.ScheduleService.Helper;


namespace SkyMINI.ScheduleService.Controllers
{
    [Route("api/[controller]")]
    public class LocationsController : Controller
    {
        private readonly ScheduleDbContext _context;
        private readonly ILocationServiceMapper _mapper;
        private readonly ILogger _logger;

        public LocationsController(ScheduleDbContext context,
                                  ILocationServiceMapper mapper,
                                  ILogger<LocationsController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAllLocations()
        {
            var locations = _context.Location;

            if (locations == null || !locations.Any())
            {
                _logger.LogInformation("Location data is null or empty.");
                return NotFound("No locations available");
            }

            else
            {
                List<LocationDTO> locationDtos = new List<LocationDTO>();

                foreach (var location in locations)
                {
                    if (location != null)
                    {
                        var locationDto = _mapper.LocationToDTO(location);
                        locationDtos.Add(locationDto);
                    }
                }

                return new JsonResult(locationDtos);
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetLocation([FromRoute] int? id)
        {
            if (!id.HasValue)
            {
                _logger.LogInformation("Invalid was null.");
                return NotFound("Non existing location specified.");
            }

            else
            {
                var location = _context.Location.FirstOrDefault(s => s.Id == id.Value);

                if (location == null)
                {
                    _logger.LogInformation("Location was null.");
                    return NotFound("Location requested is not found.");
                }

                else
                {
                    var locationDto = _mapper.LocationToDTO(location);
                    return new JsonResult(locationDto);
                }
            }
        }

        [HttpPost]
        public IActionResult AddLocation([FromBody] LocationDTO locationDto)
        {
            if (locationDto == null)
            {
                _logger.LogInformation("Location was null");
                return BadRequest("The location to add was empty");
            }

            var locationDao = _mapper.LocationToDAO(locationDto);

            _context.Location.Add(locationDao);

            try
            {
                _context.SaveChanges();
            }

            catch (DbUpdateException e) when (locationDto.Id != 0)
            {
                string message = "Can not add already on existing location, use update schedule if needed";
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                _logger.LogError(message);
                throw new InvalidLocationException(message, e);
            }

            catch (DbUpdateException e)
            {
                string message = "The locaion to add is incomplete, ensure to complete all required fields.";
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidLocationException(message, e);
            }

            //In majority of cases won't execute.
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidOperationException("The location can not be added.", e);
            }

            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateLocation([FromBody] LocationDTO locationDto)
        {
            if (locationDto == null)
            {
                _logger.LogInformation("Location was null.");
                return BadRequest("Can not update with empty location.");
            }

            var locationToUpdate = _context.Location.FirstOrDefault(s => s.Id == locationDto.Id);

            if (locationToUpdate == null)
            {
                _logger.LogInformation("Location data is null.");
                return NotFound("No location for update available.");
            }

            _context.Entry(locationToUpdate).State = EntityState.Detached;
            locationToUpdate = _mapper.LocationToDAO(locationDto);
            _context.Entry(locationToUpdate).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }

            catch (DbUpdateException e)
            {
                string message = "The location to update is incomplete, ensure to complete all required fields.";
                _logger.LogError(e.Message);
                _logger.LogError(e.InnerException.Message);
                throw new InvalidLocationException(message, e);
            }

            catch (Exception e)
            {
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidScheduleException("The location can not be updated", e);
            }

            return Ok();
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteLocation([FromRoute] int? id)
        {
            if (!id.HasValue)
            {
                _logger.LogInformation("Invalid id.");
                return BadRequest("Non existing location specified.");
            }

            var locationToDelete = _context.Location.FirstOrDefault(s => s.Id == id.Value);

            if (locationToDelete == null)
            {
                _logger.LogInformation("Location was null.");
                return NotFound("Location requested is not found.");
            }

            if (locationToDelete.ScheduleDestinationLocation.Any() ||
                locationToDelete.ScheduleOriginLocation.Any())
            {
                _logger.LogInformation("The location's id can not be deleted because it is currently used foreign key.");
                return BadRequest("Can not delete location that is currently in use for schedule .");
            }

            _context.Entry(locationToDelete).State = EntityState.Deleted;
            _context.Location.Remove(locationToDelete);

            try
            {
                _context.SaveChanges();
            }

            catch (Exception e)
            {
                _logger.LogError(e.Message);
                _logger.LogError(e?.InnerException.Message);
                throw new InvalidOperationException("Delete operation can not be performed", e);
            }

            return Ok();
        }
    }
}
