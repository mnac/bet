﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SkyMINI.ScheduleService.Models
{
    public partial class ScheduleDbContextold : DbContext
    {
        public ScheduleDbContextold()
        {
        }

        public ScheduleDbContextold(DbContextOptions<ScheduleDbContextold> options)
            : base(options)
        {
        }

        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Schedules> Schedules { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.AirportName)
                    .HasColumnName("Airport_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);
            });

            modelBuilder.Entity<Schedules>(entity =>
            {
                entity.Property(e => e.Arrival).HasColumnType("datetime");

                entity.Property(e => e.Departure).HasColumnType("datetime");

                entity.Property(e => e.DestinationLocationId).HasColumnName("Destination_Location_Id");

                entity.Property(e => e.OriginLocationId).HasColumnName("Origin_Location_Id");

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.HasOne(d => d.DestinationLocation)
                    .WithMany(p => p.SchedulesDestinationLocation)
                    .HasForeignKey(d => d.DestinationLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Schedules_ToDestinationLocation");

                entity.HasOne(d => d.OriginLocation)
                    .WithMany(p => p.SchedulesOriginLocation)
                    .HasForeignKey(d => d.OriginLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Schedules_ToOriginLocation");
            });
        }
    }
}
