﻿using System;
using System.Collections.Generic;

namespace SkyMINI.ScheduleService.Models
{
    public partial class Location
    {
        public Location()
        {
            SchedulesDestinationLocation = new List<Schedules>();
            SchedulesOriginLocation = new List<Schedules>();
        }

        public int Id { get; set; }
        public string City { get; set; }
        public string AirportName { get; set; }

        public ICollection<Schedules> SchedulesDestinationLocation { get; set; }
        public ICollection<Schedules> SchedulesOriginLocation { get; set; }
    }
}
