﻿using System;
using System.Collections.Generic;

namespace SkyMINI.ScheduleService.Models
{
    public partial class Schedules
    {
        public int Id { get; set; }
        public DateTime Departure { get; set; }
        public DateTime Arrival { get; set; }
        public int? AirPlaneId { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public int OriginLocationId { get; set; }
        public int DestinationLocationId { get; set; }

        public Location DestinationLocation { get; set; }
        public Location OriginLocation { get; set; }
    }
}
