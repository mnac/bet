﻿namespace SkyMINI.ScheduleService.Domain.Models
{
    public class LocationDTO
    {
        public int Id { get; set; }
        public string AirportCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string AirportName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
