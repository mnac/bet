﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyMINI.ScheduleService.Domain.Models
{
    public enum Status
    {
        Unkonwn,
        NotAvailable,
        OnFly,
        Delayed,
        InTime,
        Cancelled,
    }
}
