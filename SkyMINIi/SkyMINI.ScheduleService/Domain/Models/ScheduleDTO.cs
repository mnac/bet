﻿using System;
using AirplaneService.Models.DB;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SkyMINI.ScheduleService.Domain.Models
{
    public class ScheduleDTO
    {
        public int Id { get; set; }
        public DateTime Departure { get; set; }
        public DateTime Arrival { get; set; }

        public Airplane Airplane { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }

        public LocationDTO DestinationLocation { get; set; }
        public LocationDTO OriginLocation { get; set; }
    }
}
