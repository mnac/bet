﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyMINI.ScheduleService
{
    [Serializable]
    public class InvalidScheduleException : Exception
    {
        public InvalidScheduleException(){ }

        public InvalidScheduleException(string message) : base(message){ }

        public InvalidScheduleException(string message, Exception inner) : base(message, inner){ }
    }
}
