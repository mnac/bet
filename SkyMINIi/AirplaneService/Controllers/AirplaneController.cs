﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using AirplaneService.Models.DB;
using log4net;

namespace AirplaneService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirplaneController : ControllerBase
    {
        private readonly MicroserviceDatabaseContext context;
        private static readonly ILog logger = LogManager.GetLogger(typeof(AirplaneController));

        public AirplaneController(MicroserviceDatabaseContext dbcont)
        {
            context = dbcont;
        }

        [HttpPut]
        public IActionResult Update([FromBody] Airplane airplane)
        {
            logger.Info("************************Update started************************");
            if (airplane == null)
            {
                logger.Info("Update:Airplane was null");
                return BadRequest("Can't update empty airplane");
            }
            var UpdateAirplane = context.Airplane.FirstOrDefault(a => a.AirplaneId == airplane.AirplaneId);

            if (UpdateAirplane == null)
            {
                logger.Info(string.Format("Update:No matching airplane found with Id:{0}", airplane.AirplaneId));
                return NotFound("The requested airplane was not found");
            }
            UpdateAirplane.Alcohol = airplane.Alcohol;
            UpdateAirplane.Lunch = airplane.Lunch;
            UpdateAirplane.MaxSpeed = airplane.MaxSpeed;
            UpdateAirplane.MemberCount = airplane.MemberCount;
            UpdateAirplane.Model = airplane.Model;
            UpdateAirplane.SeatCount = airplane.SeatCount;
            UpdateAirplane.Status = airplane.Status;
            context.Entry(UpdateAirplane).State = EntityState.Modified;
            try
            {
                context.Update(UpdateAirplane);
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                logger.Error(ex.Message);
                throw new InvalidOperationException("The update operation can not be completed", ex);
            }
            logger.Info("The Update operation completed successfully");
            return Ok(UpdateAirplane);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            logger.Info("************************Delete started************************");
            var deleting_airplane = context.Airplane.FirstOrDefault(a => a.AirplaneId == id);
            if (deleting_airplane == null)
            {
                logger.Info(string.Format("Delete:No matching airplane found with Id:{0}", id));
                return BadRequest("Can't delete non-existing airplane");
            }
            try
            {
                context.Airplane.Remove(deleting_airplane);
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw new Exception("The delete operation can not be completed", ex);
                
            }
            logger.Info("The Delete operation completed successfully");
            return Ok(deleting_airplane);
        }

        [HttpPost]
        public IActionResult Add([FromBody] Airplane airplane)
        {
            logger.Info("************************Add started************************");
            if (airplane == null)
            {
                logger.Info("Add:Airplane was null");
                return BadRequest("Can't add empty airplane");
            }
            if (context.Airplane.Any(a => a.AirplaneId == airplane.AirplaneId))
            {
                logger.Info("Add:The Id is being used");
                return Conflict("Airplane with the same Id already exits");
            }
            try
            {
                context.Airplane.Add(airplane);
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw new Exception("The Add operation can not be completed", ex);
            }
            logger.Info("The Add operation completed successfully");
            return Ok(airplane);
        }

        [HttpGet("{id:int}")]
        public IActionResult Get([FromRoute] int id)
        {
            logger.Info("************************Get started************************");
            var airplane = context.Airplane.FirstOrDefault(a => a.AirplaneId == id);
            if (airplane == null)
            {
                logger.Info("GET:Airplane was not found");
                return NotFound(string.Format("The requested airplane with ID:{0} is not found", id));
            }
            var result = new JsonResult(airplane);
            logger.Info("The Get operation completed successfully");
            return result;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            logger.Info("************************GetAll started************************");
            var airplanes = context.Airplane.ToList();

            if (airplanes == null)
            {
                logger.Info("GETALL:Airplane data is empty");
                return NotFound("No airplane available");
            }
            logger.Info("The GetAll operation completed successfully");
            return new JsonResult(airplanes);
        }
    }
}