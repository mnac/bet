﻿using System;
using System.Collections.Generic;

namespace AirplaneService.Models.DB
{
    public partial class Airplane
    {
        public int AirplaneId { get; set; }
        public string Model { get; set; }
        public int SeatCount { get; set; }
        public int MaxSpeed { get; set; }
        public string MemberCount { get; set; }
        public string Status { get; set; }
        public bool Lunch { get; set; }
        public bool Alcohol { get; set; }
    }
}
