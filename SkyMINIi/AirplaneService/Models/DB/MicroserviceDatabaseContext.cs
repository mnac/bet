﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AirplaneService.Models.DB
{
    public partial class MicroserviceDatabaseContext : DbContext
    {
        public MicroserviceDatabaseContext()
        {
        }

        public MicroserviceDatabaseContext(DbContextOptions<MicroserviceDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Airplane> Airplane { get; set; }

  
       
        /*   protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
           {
               if (!optionsBuilder.IsConfigured)
               {
   #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                   optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=MicroserviceDatabase;Trusted_Connection=True;");
               }
           }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Airplane>(entity =>
            {
                entity.Property(e => e.AirplaneId).ValueGeneratedNever();

                entity.Property(e => e.MemberCount)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
