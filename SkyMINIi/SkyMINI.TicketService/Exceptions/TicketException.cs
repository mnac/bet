﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyMINI.TicketService.Exceptions
{
    public class TicketException : Exception
    {
        public TicketException()
        {
        }

        public TicketException(string message) : base(message)
        {
        }

        public TicketException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
