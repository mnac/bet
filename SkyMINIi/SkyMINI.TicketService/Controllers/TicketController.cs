﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SkyMINI.TicketService.Mapper;
using SkyMINI.TicketService.Models.DAL;
using SkyMINI.TicketService.Models.BL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SkyMINI.TicketService.Exceptions;

namespace SkyMINI.TicketService.Controllers
{ 
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly TicketServiceDBContext context;
        private readonly ITicketServiceMapper mapper;
        private readonly ILogger logger; 

        public TicketController(TicketServiceDBContext context, ITicketServiceMapper mapper,ILogger<TicketController> logger)
        {
            this.context = context;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            logger.LogInformation("Get all Tickets");
            var ticketDALs = context.Ticket.ToList();

            if (ticketDALs == null||!ticketDALs.Any())
            {
                logger.LogWarning("Tickets not found");
                return NotFound();
            }

            List<TicketBL> ticketBLs = new List<TicketBL>();

            foreach (var t in ticketDALs)
            {
                var tempTicketBL = mapper.TicketToBL(t);
                if (tempTicketBL != null)
                    ticketBLs.Add(tempTicketBL);
            }

            return new JsonResult(ticketBLs);
        }

        [HttpGet("{id}")]
        public IActionResult GetTicketById(int id)
        {
            logger.LogInformation("Get Ticket By ID {ID}", id);

            var ticket = context.Ticket.FirstOrDefault(t => t.TicketId == id);
            if (ticket == null)
            {
                logger.LogWarning("GetTicketById({ID}) NOT FOUND", id);
                return NotFound(id);
            }

            TicketBL ticketBL = mapper.TicketToBL(ticket);
            return new JsonResult(ticketBL);
        }

        [HttpPost]
        public IActionResult AddTicket([FromBody] TicketBL ticket)
        {

            if (ticket == null)
            {
                logger.LogWarning("Ticket is null");
                return BadRequest(ticket);
            }

            logger.LogInformation("AddTicket {0} ID ", ticket.TicketId);


            if(context.Ticket.Any(t => t.TicketId == ticket.TicketId && t.SeatNumber == ticket.SeatNumber))
            {
                logger.LogWarning("Ticket with same id are exsist ID :{ID}", ticket.TicketId);
                return Conflict(ticket);
            }

            TicketDAL ticketDAL = mapper.TicketToDAL(ticket);
            context.Ticket.Add(ticketDAL);
            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                logger.LogError(e.Message);
                logger.LogError(e?.InnerException.Message);
                throw new TicketException("The Ticket add operation is incomplete", e);
            }
            catch(Exception e)
            {
                logger.LogError(e.Message);
                logger.LogError(e?.InnerException.Message);
                throw new InvalidOperationException("Ticket can not be  added", e);
            }
            return Ok(ticket);
        }

        [HttpPut]
        public IActionResult UpdateTicket([FromBody] TicketBL ticket)
        {
            if (ticket == null)
            {
                logger.LogInformation("Ticket was null");
                return BadRequest(ticket);
            }
            var updatingTicket = context.Ticket.FirstOrDefault(t => t.TicketId == ticket.TicketId);

            if (updatingTicket == null)
                return NotFound();

            context.Entry(updatingTicket).State = EntityState.Detached;
            updatingTicket = mapper.TicketToDAL(ticket);
            context.Entry(updatingTicket).State = EntityState.Modified;
            context.Update(updatingTicket);
            try
            {
                context.SaveChanges();
            }
            catch(DbUpdateException e)
            {
                logger.LogError(e.Message);
                logger.LogError(e?.InnerException.Message);
                throw new TicketException("Ticket Update Failed ", e);
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                logger.LogError(e?.InnerException.Message);
                throw new TicketException("The ticket can not be updated", e);
            }
            TicketBL updatingTicketBL = mapper.TicketToBL(updatingTicket);
            logger.LogInformation("Ticket with {ID} Id Updated",ticket.TicketId);
            return Ok(updatingTicketBL);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTicket(int id)
        {
            var deleteTicket = context.Ticket.FirstOrDefault(t => t.TicketId == id);
            if (deleteTicket == null)
            {
                logger.LogWarning("Ticket with {ID} Id not found");
                return BadRequest();
            }
            context.Ticket.Remove(deleteTicket);

            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                logger.LogError(e.Message);
                logger.LogError(e?.InnerException.Message);
                throw new TicketException("Ticket delete Failed ", e);
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                logger.LogError(e?.InnerException.Message);
                throw new TicketException("The ticket can not be deleted", e);
            }
            TicketBL deleteTicketBL = mapper.TicketToBL(deleteTicket);
            logger.LogInformation("Ticket with {ID} Id Deleted", id);
            return Ok(deleteTicketBL);
        }
    }
}