﻿using AirplaneService.Models.DB;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SkyMINI.ScheduleService.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyMINI.TicketService.Models.BL
{
    public class TicketBL
    {
        public int TicketId { get; set; }
        public decimal Price { get; set; }
        public string TravelCompanyName { get; set; }
        public int PassengerId { get; set; }
        public ScheduleDTO Schedule { get; set; }
        public bool IsAdult { get; set; }
        public bool IsOneWay { get; set; }
        public Airplane Airplane { get; set; }
        public string SeatNumber { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public SeatClass SeatClass { get; set; }

    }
}
