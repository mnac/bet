﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SkyMINI.TicketService.Models.DAL
{
    public partial class TicketServiceDBContext : DbContext
    {
        public TicketServiceDBContext()
        {
        }

        public TicketServiceDBContext(DbContextOptions<TicketServiceDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TicketDAL> Ticket { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer("Data Source=.\\SQLEXPRESS;Initial Catalog=TicketServiceDB;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TicketDAL>(entity =>
            {


                entity.Property(e => e.Price).HasColumnType("decimal(8, 2)");


                entity.Property(e => e.SeatNumber)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.TravelCompanyName)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
