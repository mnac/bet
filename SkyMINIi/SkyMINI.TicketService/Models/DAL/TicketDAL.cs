﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SkyMINI.TicketService.Models.DAL
{
    public partial class TicketDAL
    {
        [Key]
        public int TicketId { get; set; }
        public decimal Price { get; set; }
        public string TravelCompanyName { get; set; }
        public int PassengerId { get; set; }
        public int ScheduleId { get; set; }
        public bool IsAdult { get; set; }
        public bool IsOneWay { get; set; }
        public int AirplaneId { get; set; }
        public string SeatNumber { get; set; }
        public string SeatClass { get; set; } 
    }
}
