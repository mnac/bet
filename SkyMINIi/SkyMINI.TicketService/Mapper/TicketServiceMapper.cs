﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AirplaneService.Models.DB;
using SkyMINI.ScheduleService.Domain.Models;
using SkyMINI.TicketService.Models.BL;
using SkyMINI.TicketService.Models.DAL;
namespace SkyMINI.TicketService.Mapper
{
    public class TicketServiceMapper : ITicketServiceMapper
    {
        static HttpClient client = new HttpClient();
        public TicketBL TicketToBL(TicketDAL ticket)
        {
            client.BaseAddress = new Uri("http://localhost:44303/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            
            Airplane airplane = null;
            HttpResponseMessage airlaneResponse = client.GetAsync("api/airplane/"+ticket.AirplaneId.ToString()).Result;
            if (airlaneResponse.IsSuccessStatusCode)
            {
                airplane =  airlaneResponse.Content.ReadAsAsync<Airplane>().Result;
            }


            client.BaseAddress = new Uri("http://localhost:57815/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            ScheduleDTO schedule= null;
            HttpResponseMessage scheduleResponse = client.GetAsync("api/schedule/" + ticket.ScheduleId.ToString()).Result;
            if (scheduleResponse.IsSuccessStatusCode)
            {
                schedule = scheduleResponse.Content.ReadAsAsync<ScheduleDTO>().Result;
            }


            bool statusParsed = Enum.TryParse(typeof(SeatClass), ticket.SeatClass, out var result);

            TicketBL ticketBL = new TicketBL
            {
                TicketId = ticket.TicketId,
                TravelCompanyName = ticket.TravelCompanyName,
                SeatNumber = ticket.SeatNumber,
                Airplane = airplane,
                IsAdult = ticket.IsAdult,
                IsOneWay = ticket.IsOneWay,
                PassengerId = ticket.PassengerId,
                Price = ticket.Price,
                Schedule = schedule,
                SeatClass = (SeatClass) result
            };
            return ticketBL;
        }

        public TicketDAL TicketToDAL(TicketBL ticket)
        {
            TicketDAL ticketDAL = new TicketDAL
            {
                TicketId = ticket.TicketId,
                TravelCompanyName = ticket.TravelCompanyName,
                SeatNumber = ticket.SeatNumber,
                AirplaneId = ticket.Airplane.AirplaneId,
                IsAdult = ticket.IsAdult,
                IsOneWay = ticket.IsOneWay,
                PassengerId = ticket.PassengerId,
                Price = ticket.Price,
                ScheduleId = ticket.Schedule.Id,
                SeatClass = ticket.SeatClass.ToString()
            };
            return ticketDAL;
        }
    }
}
