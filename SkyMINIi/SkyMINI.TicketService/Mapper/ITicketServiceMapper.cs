﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SkyMINI.TicketService.Models.BL;
using SkyMINI.TicketService.Models.DAL;
namespace SkyMINI.TicketService.Mapper
{
    public interface ITicketServiceMapper
    {
        TicketDAL TicketToDAL(TicketBL ticket);

        TicketBL TicketToBL(TicketDAL ticket);
    }
}
