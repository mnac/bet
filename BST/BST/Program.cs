﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BST
{
    class Program
    {
        static void Main(string[] args)
        {
            BST tree = new BST(10);
            tree.Insert(15);
            tree.Insert(16);
            tree.Insert(25);
            tree.Insert(42);
            tree.Insert(111);
            tree.Insert(450);
            tree.PrintByLevels();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            BST.Ballance(ref tree);
            tree.PrintByLevels();
        }
    }
    class BST
    {
        BST left;
        BST right;
        int data;
        public BST(int Data)
        {
            data = Data;
        }
        public static void PrintInOrder(BST t)
        {
            if (t != null)
            {
                Console.WriteLine(t.data);
                PrintInOrder(t.left);
                PrintInOrder(t.right);
            }
        }
        public void Insert(int Data)
        {
            if (Data >= data)
            {
                if (right != null)
                {
                    right.Insert(Data);
                    return;
                }
                else right = new BST(Data);
            }
            if (Data < data)
            {
                if (left != null)
                {
                    left.Insert(Data);
                    return;
                }
                else left = new BST(Data);
            }
        }
        private static void NodeToList(List<int> list, BST tree)
        {
            if (tree != null)
            {
                list.Add(tree.data);
                NodeToList(list, tree.left);
                NodeToList(list, tree.right);
            }
        }
        public static void Ballance(ref BST Tree)
        {
            List<int> temp = new List<int>();
            NodeToList(temp, Tree);
            temp.Sort();
            Tree = new BST(temp[temp.Count/2]);
            for (int i = 0; i < temp.Count; i++)
            {
                if (i == temp.Count / 2) continue;
                Tree.Insert(temp[i]);
            }
            }
        public void PrintByLevels()
        {
            BST temp;
            Queue<BST> queue = new Queue<BST>();
            queue.Enqueue(this);
            while (true)
            {

                int count = queue.Count;
                
                if (count == 0) break;
                while (count > 0)
                {
                    temp = queue.Dequeue();
                    Console.Write(temp.data+"   ");
                    if (temp.left != null) queue.Enqueue(temp.left);
                    if (temp.right != null) queue.Enqueue(temp.right);
                    count--;
                }
                Console.WriteLine();
            }
        }

    }

}
