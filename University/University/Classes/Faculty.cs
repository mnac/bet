﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    class Faculty:IDisplayable
    {
        private string _name;
        private DateTime _creationDate;
        private List<Student> _students;
        private List<Lecturer> _lecturers;

        public string Name{
            get { return _name; }
            set { if (value.Length > 0 && value.Length < 50) _name = value; }
        }
        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { if (value.Year <= 2019) _creationDate = value; }
        }
        public Faculty()
        {
            _students = new List<Student>();
            _lecturers = new List<Lecturer>();
        }

        public Faculty(string name,DateTime date)
        {
             Name = name;
             CreationDate = date;
            _students = new List<Student>();
            _lecturers = new List<Lecturer>();
        }


        public int Expel(int ID)
        {
            Student st= _students.FirstOrDefault(x => x.ID == ID);
            if (st != null)
            {
                if (_students.Remove(st)) return 0;
                return -1;
            }

            Lecturer lr = _lecturers.FirstOrDefault(x => x.ID == ID);
            if (lr != null)
            {
                if (_lecturers.Remove(lr)) return 0;
                return -1; 
            }

            return -1;

        }
        public int Add(Person person)
        {
            if(person is Student student)
            {
                _students.Add(student);
                return 0;
            }
            else if(person is Lecturer lecturer)
            {
                _lecturers.Add(lecturer);
                return 0;
            }
            return -1;
        }
        public void Display()
        {
            Console.WriteLine("******Information about faculty******");
            Console.WriteLine($"Name:          {Name}");
            Console.WriteLine($"Creation date: {CreationDate.ToString()}");
            Console.WriteLine("******Students******");
            foreach (Student st in _students)
            {
                Console.WriteLine(st.FullName);
            }
            Console.WriteLine("******Lecturers******");
            foreach (Lecturer lt in _lecturers)
            {
                Console.WriteLine(lt.FullName);
            }
            Console.WriteLine();
        }
    }
}
