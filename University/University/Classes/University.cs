﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    class University:IDisplayable
    {

        private string _name { get; set; }
        private DateTime _creationDate;
        private Dictionary<string, Faculty> _faculties;
         
        public string Name
        {
            get { return _name; }
            set { if (value.Length > 0 && value.Length < 35) _name = value; }
        }

        public DateTime CreationDate
        {    
            get { return _creationDate; }
            set { if (value.Year <= 2019) _creationDate = value; }
        }

        public University()
        {
             Name = "NoName";
             CreationDate = DateTime.Now;
            _faculties = new Dictionary<string, Faculty>();
        }

        public University(string name,DateTime creationdate)
        {
            Name = name;
            CreationDate = creationdate;
            _faculties = new Dictionary<string, Faculty>();
        }

         public int AddFaculty(Faculty faculty)
        {
            if (_faculties.Keys.Contains(faculty.Name)) { Console.WriteLine("Faculty already exists"); return -1; }

            _faculties.Add(faculty.Name,faculty);
            return 0;
        }

        public Faculty GetFaculty(string name)
        {

            return _faculties.Keys.Contains(name) ? _faculties[name] : null;
        }
         
        public void Display()
        {
            Console.WriteLine("**********Information about university*********");
            Console.WriteLine($"Name:          {Name}");
            Console.WriteLine($"Creation date: {CreationDate.ToString()}");
            Console.WriteLine("*****Faculties*****");
            foreach (string name in _faculties.Keys)
            {
                Console.WriteLine("-"+name);
            }
            Console.WriteLine();
        }
    }
}
