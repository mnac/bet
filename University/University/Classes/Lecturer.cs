﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    class Lecturer:Person
    {
        private string _department;
        private int _salary;
        private string _rank;

        public string Department{
            get { return _department; }
            set { if (value.Length > 0 && value.Length < 60) _department = value; }
        }
        public int Salary{
            get { return _salary; }
            set { if (value > 0) _salary = value; }
        }
        public string Rank{
            get { return _rank; }
            set { if (value.Length > 0 && value.Length < 20) _rank = value; }
        }
        public Lecturer() { }
        public Lecturer(string fullname, int age, char sex, string address, string department, int salary,string rank) 
            : base(fullname, age, sex, address)
        { Department = department; Salary = salary; Rank = rank; }

        public override void Display()
        {
            base.Display();
            Console.WriteLine($"Department: {Department}");
            Console.WriteLine($"Salary:     {Salary}");
            Console.WriteLine($"Rank:       {Rank}\n");
            Console.WriteLine();
        }
    }
}
