﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    class Student:Person
    {

        private int _course;
        private int _group;
        public int Course{
            get { return _course; }
            set { if (value >= 1 && value <=4) _course = value; }
        }
        public int Group{
            get { return _group; }
            set { if (value >= 1 && value <= 10) _group = value; }
        }
        public Student() { }
        public Student(string fullname, int age, char sex, string address,int course,int group)
            :base(fullname, age, sex, address)
        { Course = course; Group = group; }
    
        
        public override void Display()
        {
            base.Display();
            Console.WriteLine($"Course:     {Course}");
            Console.WriteLine($"Group:      {Group}\n");
            Console.WriteLine();
        }
    }
}