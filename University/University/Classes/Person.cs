﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
   abstract class Person:IDisplayable
    {
        private static int _count=0;
        private string _fullname;
        private int _age;
        private char _sex;
        private string _address;

        public int ID { get; }
        public string FullName{
            get { return _fullname; }
            set { if (value.Length > 0 && value.Length < 30) _fullname = value; }
        }
        public int Age{
            get { return _age; }
            set { if (value > 0 && value < 100) _age = value; }
        }
        public char Sex{
            get { return _sex; }
            set { if (value == 'm' || value == 'M' || value == 'f' || value == 'F') _sex = value; }
        }
        public string Address{
            get { return _address; }
            set { if (value.Length > 0 && value.Length < 40) _address = value; }
        }
        public Person() { ID = _count++; }
        public Person(string fullname,int age,char sex,string address )
        {
            FullName = fullname;
            Age = age;
            Sex = sex;
            Address = address;
            ID = _count++;
        }
        public virtual void Display()
        {
            Console.WriteLine("\n******Information about person******");
            Console.WriteLine($"ID:         {ID}");
            Console.WriteLine($"Full name:  {FullName}");
            Console.WriteLine($"Age:        {Age}");
            Console.WriteLine($"Sex:        {Sex}");
            Console.WriteLine($"Address:    {Address}");
             
        }

    }
}
