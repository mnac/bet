﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] people = { new Student { FullName="Name1",Address="Nor Nork",Age=19,Course=2,Group=1,Sex='M'},
                                new Student { FullName="Name2",Address="Nork marash",Age=20,Course=3,Group=3,Sex='F'},
                                new Student { FullName="Name3",Address="Sari tagh",Age=21,Course=4,Group=9,Sex='M'},
                                new Lecturer { FullName="Name4",Address="Zeytun",Age=66,Department="Algebra",Salary=120000, Rank="Docent",Sex='M'},
                                new Lecturer { FullName="Name5",Address="davtashen",Age=78,Department="programming",Salary=150000, Rank="Doctor",Sex='F'}
            };
            people[0].Display();

            University YSU = new University("Yerevan State University", new DateTime(1919, 5, 21));
            Faculty ikm=new Faculty("Informatics and applied mathematics", new DateTime(1961, 4, 1));
            foreach (Person p in people)
            {
                ikm.Add(p);
            }
             
            YSU.AddFaculty(ikm);
            YSU.AddFaculty(new Faculty("Sociology", new DateTime(1999, 1, 3)));
            YSU.AddFaculty(new Faculty("Managemant", new DateTime(1947, 5, 4)));

            Faculty Man = YSU.GetFaculty("Managemant");
            foreach (Person p in people)
            {
                Man?.Add(p);//or if(Man!=null)...
            }

            //Display some info about this university
            YSU.Display();
            
            //about faculty of managemant
            Man.Display();

            Man.Expel(1);
            Console.WriteLine("***After Expelling***");
            Man.Display();

            Console.ReadLine();
        }
    }
}
