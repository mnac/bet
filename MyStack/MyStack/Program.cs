﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    class MyStack<T>
    {
        int currIndex = -1;
        T[] arr;
        public MyStack(int capacity)
        {
            arr = new T[capacity];
        }
        public void Push(T data)
        {
            if (IsFull())
                throw new Exception("Stack is full");
            arr[++currIndex] = data;
        }
        public T Pop()
        {
            if (IsEmpty())
                throw new Exception("Stack is Empty");
            return arr[currIndex--];
        }
        public void Push_Bottom(T data)
        {
            if (currIndex == 0)
            {
                T temp = Pop();
                Push(data);
                Push(temp);
            }
            else
            {
                T temp = Pop();
                Push_Bottom(data);
                Push(temp);
            }
        }
        public int Size() => currIndex + 1;
        public bool IsEmpty() => currIndex == -1;
        public bool IsFull() => currIndex == arr.Length - 1;
        public T Top()
        {
            if (IsEmpty())
                throw new Exception("Stack is Empty");
            return arr[currIndex];
        }
    }
    class SortStack
    {
        int currIndex = -1;
        int max;
        int[] arr;
        public SortStack(int capacity)
        {
            arr = new int[capacity];
        }
        public void Push(int data)
        {
            if (IsFull()) throw new Exception("Stack is full");

            if (currIndex == -1) { max = data; arr[++currIndex] = data; }
            else if (data > max)
            {
                arr[++currIndex] = 2 * data - max;
                max = data;
            }

            else arr[++currIndex] = data;
        }

        public int Pop()
        {
            if (IsEmpty())
                throw new Exception("Stack is Empty");

            int removed = arr[currIndex];
            int oldmax = max;
            if (removed > max)
            {
                max = 2 * max - removed;
                currIndex--;
                return oldmax;
            }
            return arr[currIndex--];
        }
        public int Max() => max;

        public int Size() => currIndex + 1;
        public bool IsEmpty() => currIndex == -1;
        public bool IsFull() => currIndex == arr.Length - 1;
        public int Top()
        {
            if (IsEmpty())
                throw new Exception("Stack is Empty");
            return arr[currIndex];
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            SortStack stack = new SortStack(10);
            stack.Push(10);
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(-22);
            Console.WriteLine(stack.Max());
            stack.Push(20);
            stack.Push(3);
            stack.Push(2);
            Console.WriteLine(stack.Max());
            stack.Push(30);
            Console.WriteLine(stack.Max());
            stack.Pop();
            stack.Pop();
            stack.Pop();
            stack.Pop();
            Console.WriteLine(stack.Max());
        }
    }
}
