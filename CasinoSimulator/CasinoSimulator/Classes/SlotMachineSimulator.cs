﻿using System;
using CasinoSimulator.Interfaces;

namespace CasinoSimulator
{
    class SlotMachineSimulator : ISlotSimulation
    {
        private Random _random;
        public SlotMachineSimulator()
        {
            _random = new Random();
        }
        public int[] Spin(int Probability)
        {
            int _n1 = 0, _n2 = 0, _n3 = 0;
            switch (Probability)
            {
                case 15:
                    {
                        //I did some calculations and found out that using this algorithm probability would be about 15%
                        _n1 = _random.Next(0, 9);
                        _n2 = _random.Next(_n1 - 1, _n1 + 2);
                        _n3 = _random.Next(_n1 - 1, _n1 + 1);
                    }
                    break;
                case 50:
                    {
                        //I did some calculations and found out that using this algorithm probability would be about 50%
                        _n1 = _random.Next(0, 9);
                        _n2 = _random.Next(_n1 - 1, _n1 + 1);
                        _n3 = _random.Next(_n2, _n2 + 1);
                        //

                    }
                    break;
                case 80:
                    {
                        //I did some calculations and found out that using this algorithm probability would be about 80%
                        _n1 = _random.Next(0, 9);
                        if (_n1 > 6)
                        {
                            _n2 = _random.Next(_n1 - 1, _n1 + 1);
                            _n3 = _n1;
                        }
                        else
                        {
                            _n2 = _random.Next(_n1, _n1 + 1);
                            _n3 = _n1;
                        }
                    }
                    break;
                default:
                    break;
            }
            _n2 = Math.Abs(_n2);
            _n3 = Math.Abs(_n3);
            if (_n2 > 9) _n2 = 9;
            if (_n3 > 9) _n3 = 9;
            return new int[] { _n1, _n2, _n3 };
        }
    }
}
