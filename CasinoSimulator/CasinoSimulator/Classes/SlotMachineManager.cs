﻿using CasinoSimulator.Interfaces;
using System.Windows.Forms;
namespace CasinoSimulator
{
    class SlotMachineManager : ISlotManager
    {
        private ISlotSimulation _simulator;
        public int[] numbers { get; set; }
        public int Credits { set; get; } = 100;
        public int Bet { set; get; }
        public int Probability { set; get; }
        public SlotMachineManager(int credits, ISlotSimulation simulator)
        {
            Credits = credits;
            _simulator = simulator;
        }
        public int RunSimulation()
        {
            if (Credits - Bet >= 0)
            {
                numbers = _simulator.Spin(Probability);
                Credits -= Bet;
                return 0;
            }
            else { MessageBox.Show("You don't have enough money"); return -1; }

        }
        public bool Win()
        {
            if (numbers[0] == numbers[1] && numbers[1] == numbers[2]) return true;
            return false;
        }

        public bool AskPlayerToPlayOrExit()
        {
            DialogResult result = MessageBox.Show("Do you want to continue?", "Continue Or Exit", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes) return true;
            return false;
        }
    }
}
