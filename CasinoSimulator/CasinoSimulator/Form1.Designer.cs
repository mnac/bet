﻿namespace CasinoSimulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Credit_lbl = new System.Windows.Forms.Label();
            this.Bet_lbl = new System.Windows.Forms.Label();
            this.Betbox = new System.Windows.Forms.ListBox();
            this.fifteen_btn = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.eight_btn = new System.Windows.Forms.RadioButton();
            this.fifty_btn = new System.Windows.Forms.RadioButton();
            this.display_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.IndianRed;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Lavender;
            this.label1.Location = new System.Drawing.Point(196, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 135);
            this.label1.TabIndex = 0;
            this.label1.Text = "?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.IndianRed;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Lavender;
            this.label4.Location = new System.Drawing.Point(326, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 135);
            this.label4.TabIndex = 3;
            this.label4.Text = "?";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.IndianRed;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Lavender;
            this.label5.Location = new System.Drawing.Point(456, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 135);
            this.label5.TabIndex = 4;
            this.label5.Text = "?";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(336, 359);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 36);
            this.button1.TabIndex = 5;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Credit_lbl
            // 
            this.Credit_lbl.AutoSize = true;
            this.Credit_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Credit_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Credit_lbl.ForeColor = System.Drawing.Color.Red;
            this.Credit_lbl.Location = new System.Drawing.Point(12, 9);
            this.Credit_lbl.Name = "Credit_lbl";
            this.Credit_lbl.Size = new System.Drawing.Size(109, 31);
            this.Credit_lbl.TabIndex = 6;
            this.Credit_lbl.Text = "Credits:";
            // 
            // Bet_lbl
            // 
            this.Bet_lbl.AutoSize = true;
            this.Bet_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Bet_lbl.Font = new System.Drawing.Font("Megen Rus", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Bet_lbl.ForeColor = System.Drawing.Color.Red;
            this.Bet_lbl.Location = new System.Drawing.Point(580, 15);
            this.Bet_lbl.Name = "Bet_lbl";
            this.Bet_lbl.Size = new System.Drawing.Size(64, 32);
            this.Bet_lbl.TabIndex = 7;
            this.Bet_lbl.Text = "Bet:";
            // 
            // Betbox
            // 
            this.Betbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Betbox.FormattingEnabled = true;
            this.Betbox.ItemHeight = 31;
            this.Betbox.Items.AddRange(new object[] {
            "10",
            "20",
            "30"});
            this.Betbox.Location = new System.Drawing.Point(641, 15);
            this.Betbox.Name = "Betbox";
            this.Betbox.Size = new System.Drawing.Size(70, 97);
            this.Betbox.TabIndex = 8;
            // 
            // fifteen_btn
            // 
            this.fifteen_btn.AutoSize = true;
            this.fifteen_btn.BackColor = System.Drawing.Color.Transparent;
            this.fifteen_btn.Checked = true;
            this.fifteen_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fifteen_btn.ForeColor = System.Drawing.Color.Red;
            this.fifteen_btn.Location = new System.Drawing.Point(18, 360);
            this.fifteen_btn.Name = "fifteen_btn";
            this.fifteen_btn.Size = new System.Drawing.Size(86, 35);
            this.fifteen_btn.TabIndex = 9;
            this.fifteen_btn.TabStop = true;
            this.fifteen_btn.Text = "15%";
            this.fifteen_btn.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(12, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 31);
            this.label6.TabIndex = 12;
            this.label6.Text = "Probability:";
            // 
            // eight_btn
            // 
            this.eight_btn.AutoSize = true;
            this.eight_btn.BackColor = System.Drawing.Color.Transparent;
            this.eight_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.eight_btn.ForeColor = System.Drawing.Color.Red;
            this.eight_btn.Location = new System.Drawing.Point(201, 360);
            this.eight_btn.Name = "eight_btn";
            this.eight_btn.Size = new System.Drawing.Size(86, 35);
            this.eight_btn.TabIndex = 13;
            this.eight_btn.TabStop = true;
            this.eight_btn.Text = "80%";
            this.eight_btn.UseVisualStyleBackColor = false;
            // 
            // fifty_btn
            // 
            this.fifty_btn.AutoSize = true;
            this.fifty_btn.BackColor = System.Drawing.Color.Transparent;
            this.fifty_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fifty_btn.ForeColor = System.Drawing.Color.Red;
            this.fifty_btn.Location = new System.Drawing.Point(110, 360);
            this.fifty_btn.Name = "fifty_btn";
            this.fifty_btn.Size = new System.Drawing.Size(86, 35);
            this.fifty_btn.TabIndex = 14;
            this.fifty_btn.TabStop = true;
            this.fifty_btn.Text = "50%";
            this.fifty_btn.UseVisualStyleBackColor = false;
            // 
            // display_lbl
            // 
            this.display_lbl.AutoSize = true;
            this.display_lbl.BackColor = System.Drawing.Color.Transparent;
            this.display_lbl.Font = new System.Drawing.Font("Megen Rus", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.display_lbl.ForeColor = System.Drawing.Color.Red;
            this.display_lbl.Location = new System.Drawing.Point(229, 398);
            this.display_lbl.Name = "display_lbl";
            this.display_lbl.Size = new System.Drawing.Size(340, 32);
            this.display_lbl.TabIndex = 17;
            this.display_lbl.Text = "HUGE WINS AWAIT YOU!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(772, 461);
            this.Controls.Add(this.display_lbl);
            this.Controls.Add(this.fifty_btn);
            this.Controls.Add(this.eight_btn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.fifteen_btn);
            this.Controls.Add(this.Betbox);
            this.Controls.Add(this.Bet_lbl);
            this.Controls.Add(this.Credit_lbl);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Credit_lbl;
        private System.Windows.Forms.Label Bet_lbl;
        private System.Windows.Forms.ListBox Betbox;
        private System.Windows.Forms.RadioButton fifteen_btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton eight_btn;
        private System.Windows.Forms.RadioButton fifty_btn;
        private System.Windows.Forms.Label display_lbl;
    }
}

