﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoSimulator.Interfaces
{
    interface ISlotSimulation
    {
        int[] Spin(int probability);
    }
}
