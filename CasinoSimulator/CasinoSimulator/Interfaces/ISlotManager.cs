﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoSimulator.Interfaces
{
    interface ISlotManager
    {
        int[] numbers { get; set; }
        int Credits { set; get; }
        int Bet { set; get; }
        int Probability { set; get; }
        bool Win();
        int RunSimulation();
        bool AskPlayerToPlayOrExit();
    }
}
