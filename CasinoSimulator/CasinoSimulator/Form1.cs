﻿using System;
using System.Windows.Forms;

namespace CasinoSimulator
{
    public partial class Form1 : Form
    {
        SlotMachineManager Manager;
        //to count number of attempts
        int count = 1;
        public Form1()
        {
            InitializeComponent();
            Manager = new SlotMachineManager(100,new SlotMachineSimulator());
            Credit_lbl.Text = string.Format($"Credits:{Manager.Credits}");
            Betbox.SelectedItem = "10";
            Manager.Bet = 10;
        }
        
        private void Form1_Load(object sender, EventArgs e) { }
        private void Label5_Click(object sender, EventArgs e){ }

        private void Button1_Click(object sender, EventArgs e)
        {
            //here we select probability's and bet's values 
            int pr = fifteen_btn.Checked ? 15 : 50;
            pr = eight_btn.Checked ? 80 : pr;
            Manager.Probability = pr;
            Manager.Bet = int.Parse(Betbox.SelectedItem.ToString());
          
            //running simulation...
            if (Manager.RunSimulation()==-1) Application.Exit();
            
            //displaying outcoming numbers
            label1.Text = Manager.numbers[0].ToString();
            label4.Text = Manager.numbers[1].ToString();
            label5.Text = Manager.numbers[2].ToString();
            

            if (Manager.Win())
            {   //if player win...
                Manager.Credits += 5 * Manager.Bet;
                display_lbl.Text = string.Format($"YOU WIN {5 * Manager.Bet}");
            }
            else //or lose...
                display_lbl.Text = string.Format($"TRY AGAIN!");

             Credit_lbl.Text = string.Format($"Credits:{Manager.Credits}");
            //After every 5 attempts SlotMachine will ask player 
            if (count % 5 == 0) { if (!Manager.AskPlayerToPlayOrExit()) Application.Exit(); }
            count++;
        }

         
    }
}
